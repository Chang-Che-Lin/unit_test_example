#include <string.h>
#include "test_framework_define.hpp"
#if (TEST_FRAMEWORK == DOCTEST)
#include <doctest/doctest.h>
#include <doctest/trompeloeil.hpp>

#elif (TEST_FRAMEWORK == CATCH2)
#include <catch2/catch_all.hpp>
#include <catch2/trompeloeil.hpp>
#include <catch2/matchers/catch_matchers_string.hpp>
#endif

#include "logic/data_process.h"
#include "mock/math_test.hpp"
#include "common/utility.h"

#if (TEST_FRAMEWORK == CATCH2)
using Catch::Matchers::Equals;
#endif

SCENARIO("compare two array")
{
    GIVEN("a array is greater than b array")
    {
        uint8_t a[4] = {0x00, 0x00, 0x00, 0x60};
        uint8_t b[4] = {0x00, 0x00, 0x00, 0x50};
        int result = 0;

        WHEN("call compare function")
        {
            result = Value4ByteComp_le(a, b);

            THEN("get result is equal to 1")
            {
                REQUIRE(result == 1);
            }
        }
    }

    GIVEN("a array is less than b array")
    {
        uint8_t a[4] = {0x00, 0x00, 0x00, 0x40};
        uint8_t b[4] = {0x00, 0x00, 0x00, 0x50};
        int result = 0;

        WHEN("call compare function")
        {
            result = Value4ByteComp_le(a, b);

            THEN("get result is equal to -1")
            {
                REQUIRE(result == -1);
            }
        }
    }
}

SCENARIO("test stub")
{
    GIVEN("Specify stub return 10")
    {
        int result = 0;
        ALLOW_CALL(stubMath, numberDouble(_)).RETURN(10);

        WHEN("call decrease() include stub function")
        {
            result = decrease();

            THEN("get result that stub return value decrease 1")
            {
                REQUIRE(result == 9);
            }
        }
    }
}

#if (TEST_FRAMEWORK == CATCH2)
SCENARIO("use array as dictionary")
{
    GIVEN("generate a file,and key is 'test2='")
    {
        uint8_t file[] = {"test=1234\r\n test2=abcdef\r\n test3=12abc\r\n"};
        uint8_t key[] = "test2=";
        uint8_t value[10] = {'\0'};
        std::string result;

        WHEN("value is fixed length")
        {
            dictionaryFixedLengthValue(file, key, value, 6);

            THEN("get value is equal to 'abcdef'")
            {
                result = (char *)value;
                CHECK_THAT(result, Equals("abcdef"));
            }
        }

        WHEN("value isn't fixed length")
        {
            uint32_t valueLength = 0;

            dictionaryNotFixedLengthValue(file, key, value, &valueLength);

            THEN("get value is equal to 'abcdef',and length is 6")
            {
                result = (char *)value;
                CHECK_THAT(result, Equals("abcdef"));
                REQUIRE(valueLength == 6);
            }
        }
    }
}
#endif
